const PORT = 3020;

export const server = {
  host: `http://localhost:${PORT}`,
  dataShort: `http://localhost:${PORT}/template`,
  dataLong: `http://localhost:${PORT}/extended-template`,
  images: `http://localhost:${PORT}/images/large`,
  thumbs: `http://localhost:${PORT}/images/thumbnails`,
};
