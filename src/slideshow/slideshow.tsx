import React, { useEffect, useState } from "react";
import axios from "axios";

import { ImageView } from "./imageView";
import { Carousel } from "./carousel";
import { Context, defaultState, Image } from "./context";
import { server } from "./constants";

export function Slideshow() {
  const [state, setState] = useState(defaultState);

  const init = (res: { data: Array<Image> }) =>
    setState((prev: any) => ({
      ...prev,
      activeImage: res.data[0],
      activeSlide: 0,
      images: res.data,
      loading: false,
      slideImages: res.data.slice(0, 4),
      totalSlides: Math.ceil(res.data.length / 4),
    }));

  useEffect(() => {
    axios.get(server.dataShort).then(init);
    // axios.get(server.dataLong).then(init);
  }, []);

  useEffect(() => {
    const { activeSlide, totalSlides, images } = state;

    if (activeSlide < totalSlides && activeSlide >= 0) {
      setState((prev) => ({
        ...prev,
        slideImages: images.slice(activeSlide * 4, activeSlide * 4 + 4),
      }));
    }
  }, [state.activeSlide]);

  return (
    <div data-testid="slideshow">
      {!state.loading ? (
        <Context.Provider value={{ state, setState }}>
          <ImageView />
          <Carousel />
        </Context.Provider>
      ) : (
        <>Loading...</>
      )}
    </div>
  );
}
