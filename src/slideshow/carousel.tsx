import React, { useContext, MouseEvent } from "react";
import { Context, Image, State } from "./context";
import { server } from "./constants";

export function Carousel() {
  const { state, setState }: any = useContext(Context);
  const { slideImages, activeImage, activeSlide, totalSlides } = state;

  const update = (data: any) =>
    setState((prev: State) => ({ ...prev, ...data }));

  const onSelect = (e: any) =>
    update({
      activeImage: slideImages.filter(
        (image: Image) => image.id === e.target.getAttribute("data-id")
      )[0],
    });

  const onPrev = () =>
    update({ activeSlide: activeSlide > 0 ? activeSlide - 1 : activeSlide });

  const onNext = () =>
    update({
      activeSlide:
        activeSlide < totalSlides - 1 ? activeSlide + 1 : activeSlide,
    });

  return (
    <div className="thumbnails" data-testid={"carousel"}>
      <div className="group">
        {slideImages.map(
          (
            {
              id,
              thumbnail,
            }: {
              id: string;
              thumbnail: string;
            },
            key: number
          ) => (
            <a
              href="#"
              title={id}
              key={key}
              className={activeImage.id === id ? "active" : ""}
              onClick={onSelect}
              data-testid={`thumbnail-${id}`}
            >
              <img
                src={`${server.thumbs}/${thumbnail}`}
                alt={`${id}-m`}
                data-id={id}
                width="145"
                height="121"
              />
              <span>{id}</span>
            </a>
          )
        )}
        <span
          className={`previous ${activeSlide === 0 ? "disabled" : ""}`}
          title="Previous"
          onClick={onPrev}
          data-testid="prev-btn"
        >
          Previous
        </span>
        <a
          href="#"
          className={`next ${activeSlide >= totalSlides - 1 ? "disabled" : ""}`}
          title="Next"
          onClick={onNext}
          data-testid="next-btn"
        >
          Next
        </a>
      </div>
    </div>
  );
}
