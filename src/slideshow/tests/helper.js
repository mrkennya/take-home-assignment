const createMockImage = (index) => ({
  title: `Business Site Template - ${index}`,
  cost: "45.00",
  id: `${index}`,
  description:
    "Lorem ipsum dolor sit amet, dictum et quisque aliquet malesuada at, rutrum ac nullam, elit massa facilisis",
  thumbnail: `${index}-m.jpg`,
  image: `${index}-b.jpg`,
});

const images = Array(8)
  .fill({})
  .map((_, i) => createMockImage(i));

export const mockState = {
  activeImage: images[0],
  activeSlide: 0,
  images,
  loading: false,
  slideImages: images.slice(0, 4),
  totalSlides: Math.ceil(images.length / 4),
};
