import React from "react";
import { render, screen } from "@testing-library/react";
import { ImageView } from "../imageView";
import { Context } from "../context";
import { mockState } from "./helper";

const setState = jest.fn();

beforeEach(() => {
  render(
    <Context.Provider value={{ state: mockState, setState }}>
      <ImageView />
    </Context.Provider>
  );
});

describe("<ImageView />", () => {
  it("renders", () => {
    expect(screen.getByTestId("image-view")).toMatchSnapshot();
    expect(screen.getByText("0-b.jpg")).toBeTruthy();
  });
});
