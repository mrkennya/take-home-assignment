import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import { Carousel } from "../carousel";
import { Context } from "../context";
import { mockState } from "./helper";

const setState = jest.fn((data) => data);

beforeEach(() => {
  render(
    <Context.Provider value={{ state: mockState, setState }}>
      <Carousel />
    </Context.Provider>
  );
});

describe("<Carousel />", () => {
  it("renders", () => {
    expect(screen.getByTestId("carousel")).toMatchSnapshot();
    expect(screen.getByText("0")).toBeTruthy();
    expect(screen.getByText("3")).toBeTruthy();
  });

  it("updates activeSlide on next/prev click", () => {
    screen.getByTestId("next-btn").click();
    expect(setState.mock.results[0].value()).toEqual({ activeSlide: 1 });

    screen.getByTestId("prev-btn").click();
    expect(setState.mock.results[1].value()).toEqual({ activeSlide: 0 });
  });

  it("doesn't update activeSlide out of bounds", () => {
    screen.getByTestId("prev-btn").click();
    expect(setState.mock.results[2].value()).toEqual({ activeSlide: 0 });

    screen.getByTestId("next-btn").click();
    screen.getByTestId("next-btn").click();
    expect(setState.mock.results[4].value()).toEqual({ activeSlide: 1 });
  });

  it("updates activeImage on thumbnail click", () => {
    fireEvent.click(screen.getByTestId("thumbnail-3"), {
      target: {
        getAttribute: () => "3",
      },
    });
    expect(setState.mock.results[5].value()).toEqual({
      activeImage: mockState.slideImages[3],
    });
  });
});
