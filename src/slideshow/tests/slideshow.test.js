import React from "react";
import { render, screen } from "@testing-library/react";

import { Slideshow } from "../slideshow";

describe("<Slideshow />", () => {
  test("displays loading state", async () => {
    render(<Slideshow />);
    expect(screen.getByTestId("slideshow")).toMatchSnapshot();
  });
});
