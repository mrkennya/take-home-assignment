import React, { useContext } from "react";
import { Context } from "./context";
import { server } from "./constants";

export function ImageView() {
  const {
    state: { activeImage },
  }: any = useContext(Context);

  const { image, title, description, cost, id, thumbnail } = activeImage;

  return (
    <div id="large" data-testid={"image-view"}>
      <div className="group">
        <img
          src={`${server.images}/${image}`}
          alt={title}
          width="430"
          height="360"
        />
        <div className="details">
          <p>
            <strong>Title</strong> {title}
          </p>
          <p>
            <strong>Description</strong> {description}
          </p>
          <p>
            <strong>Cost</strong> {`$${cost}`}
          </p>
          <p>
            <strong>ID #</strong> {id}
          </p>
          <p>
            <strong>Thumbnail File</strong> {thumbnail}
          </p>
          <p>
            <strong>Large Image File</strong> {image}
          </p>
        </div>
      </div>
    </div>
  );
}
