import React from "react";

export interface Image {
  title: string;
  cost: string;
  id: string;
  description: string;
  thumbnail: string;
  image: string;
}

export interface State {
  activeImage: Image;
  activeSlide: number;
  images: Array<Image>;
  loading: boolean;
  slideImages: Array<Image>;
  totalSlides: number;
}

export const defaultState = {
  activeImage: {},
  activeSlide: 0,
  images: [],
  loading: true,
  slideImages: [],
  totalSlides: 0,
};

export const Context = React.createContext({});
