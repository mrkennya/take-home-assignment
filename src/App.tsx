import React from "react";
import Slideshow from "./slideshow";
import "./styles.css";

export function App() {
  return (
    <div className="App">
      <div id="container">
        <header>Code Development Project</header>
        <div id="main" role="main">
          <Slideshow />
        </div>
        <footer>
          <a href="instructions.pdf">Download PDF Instructions</a>
        </footer>
      </div>
    </div>
  );
}
