const express = require("express");
const cors = require("cors");
const app = express();

const extendedTemplate = require("./data/extendedTemplate");
const template = require("./data/templates");

const PORT = 3020;

app.use(cors());

app.listen(PORT, () => {
  console.log(`api listening on http://localhost:${PORT}`);
});

app.get("/", (req, res) => {
  res.send(`api listening on http://localhost:${PORT}`);
});

// Data endpoints
app.get("/template", (req, res) => {
  res.send(template);
});

app.get("/extended-template", (req, res) => {
  res.send(extendedTemplate);
});

// File endpoints
app.get("/images/thumbnails/:image_id", (req, res) => {
  res.sendFile(`${__dirname}/images/thumbnails/${req.params.image_id}`);
});

app.get("/images/large/:image_id", (req, res) => {
  res.sendFile(`${__dirname}/images/large/${req.params.image_id}`);
});

app.get("/images/next", (req, res) => {
  res.sendFile(`${__dirname}/images/next.png`);
});

app.get("/images/previous", (req, res) => {
  res.sendFile(`${__dirname}/images/previous.png`);
});
