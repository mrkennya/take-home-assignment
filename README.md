# Getting Started

To run this project, first cd into the `api/` directory and run 

- `npm install`

then cd back to the top level directory to run

- `npm install`
- `npm start`

# 

To switch from template to template-extended, toggle these fetch statements in `slideshow.js`

```javascript
axios.get(server.dataShort).then(init);
// axios.get(server.dataLong).then(init);
```

# 

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode as well as the data API.
- App: http://localhost:3000

- API: http://localhost:3020


### `npm test`

Launches the test runner in the interactive watch mode.

